import React, { Component } from 'react';
import Form from 'react-jsonschema-form'
import './json-form.css';
import mySchema from './json-form.json';
import uiSchema from './json-uiform.json';

export default class JsonForm extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            schema: mySchema
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.externalList !== nextProps.externalList) {
            this.setState({ items: nextProps.externalList });
        }
    }

    handleSubmit({formData}) {
        console.log(formData);
    }

    render() {
        return (
            <Form schema={mySchema} uiSchema={uiSchema} onSubmit={this.handleSubmit} />
        )
    }
}