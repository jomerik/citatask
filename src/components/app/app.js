import React, { Component } from 'react';
import AppHeader from '../app-header';
import JsonForm from '../json-form';
import './app.css';

export default class App extends Component {


  paymentForm = (paymentform) => {
    this.setState({ paymentform });
  };



  render() {
    // const { paymentForm} = this.state;

    return (
      <div className="todo-app">
        <AppHeader />
        <div className="subHeader">
          <div className="subHeader_info"><span>AS„Citadelebanka”,Reģ.nr.40103303559 Republikaslaukums2A,Rīga,LV-1010,Latvija</span></div>
          <div className="subHeader_date"><span>{ ( new Date().toString() )}</span></div>
        </div>
        <JsonForm />
      </div>
    );
  };
}
